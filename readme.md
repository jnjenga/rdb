
An [sqlite3](https://www.sqlite.org/index.html) clone written in rust.

Goes without saying, do not use in production.

# Build and running

```
cargo run
```

# References

- [Let's build a simple database](https://cstack.github.io/db_tutorial)

# Author

[James Njenga](jnjenga.com)

## Feedback

If you have any feedback, please reach out to me via mail `me at jnjenga.com`
  
## Contributing

Contributions are always welcome!
