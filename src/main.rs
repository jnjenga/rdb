use std::io::Write;

enum StatementType
{
    Select = 0,
    Insert = 1,
    Unknown = 2
}

struct Statement
{
    stype: StatementType
}

fn main()
{
    let mut should_exit: bool = false;

    while !should_exit
    {
        print_prompt();

        let input = read_string();

        if input.trim() == ".exit"
        {
            should_exit = true;
            break;
        }

        if input.starts_with(".")
        {
            match do_meta_cmd(&input)
            {
                Ok(res) => println!("Command executed successfully"),
                Err(e) => println!("Error executing command")
            };

            continue;
        }

        // Sql statement
        let statement: Statement;

        statement = match prepare_statement(&input)
        {
            Ok(res) => res,
            Err(e) => { println!("Error: {}", e); continue } ,
        };

        match execute_statement(&statement)
        {
            Ok(res) => res,
            Err(e) => { println!("Error: {}", e); continue } ,
        }

    }
}

fn read_string() -> String
{
    let mut input = String::new();
    std::io::stdin()
        .read_line(&mut input)
        .expect("Unable to read input");

    input
}

fn print_help()
{
    println!("rdb version 0.0.0")
}

fn print_prompt()
{
    print!("db >");
    std::io::stdout().flush().unwrap();
}

fn do_meta_cmd(command_str: &String) -> Result<(), &str>
{
    match command_str.trim()
    {
        ".tables" => println!("Table meta command executed"),
        ".open"   => println!("Open meta command executed"),
        ".help"   => println!("Help meta command executed"),
        _         => println!("Unknown command"),
    }

    Ok(())
}

fn prepare_statement(statement_str: &String) -> Result<Statement, &str>
{
    let mut statement: Statement = Statement { stype: StatementType::Unknown };
    
    statement.stype = match statement_str.trim()
    {
        "INSERT" => StatementType::Insert,
        "SELECT" => StatementType::Select,
        _        => return Err("Unknown statement"),
    };

    Ok(statement)
}

fn execute_statement(statement: &Statement) -> Result<(), &str>
{
    match statement.stype
    {
        StatementType::Insert  => println!("Executing statement type INSERT"),
        StatementType::Select  => println!("Executing statement type SELECT"),
        StatementType::Unknown => return Err("Unknown statemtnt type encounterd"),
    }

    Ok(())
}
